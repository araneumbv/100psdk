<?php

namespace SDK;

use SDK\Exception\ApplicationException;
use SDK\Exception\ParameterMissingException;
use SDK\Exception\ResponseException;
use SDK\Exception\UnmetDependenciesException;

interface PublisherInterface
{
    /**
     * @return string
     */
    public function getStation();

    /**
     * @param string $station
     * @return Publisher
     */
    public function setStation($station);

    /**
     * @return \DateTime
     */
    public function getStartTime();

    /**
     * @param \DateTime $startTime
     * @return Publisher
     */
    public function setStartTime(\DateTime $startTime);

    /**
     * @return string
     */
    public function getItemCode();

    /**
     * @param string $itemCode
     * @return Publisher
     */
    public function setItemCode($itemCode);

    /**
     * @return string
     */
    public function getArtist();

    /**
     * @param string $artist
     * @return Publisher
     */
    public function setArtist($artist);

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $title
     * @return Publisher
     */
    public function setTitle($title);

    /**
     * @return \DateInterval
     */
    public function getLength();

    /**
     * @param \DateInterval $length
     * @return Publisher
     */
    public function setLength(\DateInterval $length);

    /**
     * @return string
     */
    public function getJockTitle();

    /**
     * @param string $jockTitle
     * @return Publisher
     */
    public function setJockTitle($jockTitle);

    /* publish functions */

    /**
     * @throws ParameterMissingException
     * @throws ResponseException
     */
    public function publishStationSong();

    /**
     * @throws ResponseException
     * @throws ParameterMissingException
     */
    public function publishStationNextSong();

    /**
     * @throws ResponseException
     * @throws ParameterMissingException
     */
    public function publishStation();

    /**
     * @throws ApplicationException
     * @throws ResponseException
     * @throws ParameterMissingException
     */
    public function publishSong();

    /**
     * @throws ApplicationException
     * @throws ResponseException
     * @throws ParameterMissingException
     * @throws UnmetDependenciesException
     */
    public function publishArtist();
}
