<?php

namespace SDK;

use SDK\Exception\ApplicationException;
use SDK\Exception\ParameterMissingException;
use SDK\Exception\ResponseException;
use SDK\Exception\UnmetDependenciesException;
use SDK\Util\Slug;

class Publisher implements PublisherInterface
{
    /**
     * @var string
     */
    protected $station;

    /**
     * @var \DateTime
     */
    protected $startTime;

    /**
     * @var string
     */
    protected $itemCode;

    /**
     * @var string
     */
    protected $artist;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var \DateInterval
     */
    protected $length;

    /**
     * @var string
     */
    protected $jockTitle;

    /**
     * @return string
     */
    public function getStation()
    {
        return $this->station;
    }

    /**
     * @param string $station
     * @return Publisher
     */
    public function setStation($station)
    {
        $this->station = $station;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param \DateTime $startTime
     * @return Publisher
     */
    public function setStartTime(\DateTime $startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getItemCode()
    {
        return $this->itemCode;
    }

    /**
     * @param string $itemCode
     * @return Publisher
     */
    public function setItemCode($itemCode)
    {
        $this->itemCode = $itemCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * @param string $artist
     * @return Publisher
     */
    public function setArtist($artist)
    {
        $this->artist = $artist;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Publisher
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return \DateInterval
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param \DateInterval $length
     * @return Publisher
     */
    public function setLength(\DateInterval $length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * @return string
     */
    public function getJockTitle()
    {
        return $this->jockTitle;
    }

    /**
     * @param string $jockTitle
     * @return Publisher
     */
    public function setJockTitle($jockTitle)
    {
        $this->jockTitle = $jockTitle;

        return $this;
    }

    /* publish functions */

    /**
     * @param bool $next
     * @return mixed
     * @throws ParameterMissingException
     * @throws UnmetDependenciesException
     * @throws \Exception
     */
    public function publishStationSong($next = false)
    {
        $id = null;
        $this->checkRequirements(['itemCode', 'startTime', 'station']);

        $data = [
                'song' => $this->itemCode,
                'playtime' => $this->startTime->getTimestamp(),
        ];
        if (null !== $this->jockTitle) {
            $data['dj'] = $this->jockTitle;
        }

        try {
            if ($next) {
                $id = Api::setStationNextSong($this->station, $data);

            } else {
                $id = Api::addStationSong($this->station, $data);
            }

        } catch (UnmetDependenciesException $e) {
            if ('musicdb_invalid_song_reference' === $e->getMessage()) {
                $this->publishSong();

                if ($next) {
                    $id = Api::setStationNextSong($this->station, $data);

                } else {
                    $id = Api::addStationSong($this->station, $data);
                }
//            // Do not automatically add a station
//            } elseif ('musicdb_invalid_playlist_reference' === $e->getMessage()) {
//                $this->publishStation();
            } else {
                throw $e;
            }
        }

        return $id;
    }

    /**
     * @return boolean
     * @throws ResponseException
     * @throws ParameterMissingException
     */
    public function publishStationNextSong()
    {
        return $this->publishStationSong(true);
    }

    /**
     * @return string
     * @throws ResponseException
     * @throws ParameterMissingException
     */
    public function publishStation()
    {
        $this->checkRequirements(['station']);

        $data = [
            'label' => $this->station,
            'active' => true,
            'position' => 2e8 -1,
        ];

        return Api::addStation($this->station, $data);
    }

    /**
     * @return string
     * @throws ApplicationException
     * @throws ResponseException
     * @throws ParameterMissingException
     */
    public function publishSong()
    {
        $this->checkRequirements(['itemCode', 'title', 'artist', 'length']);

        $reference = new \DateTimeImmutable();
        $endTime = $reference->add($this->length);
        $length = $endTime->getTimestamp() - $reference->getTimestamp();

        $data = [
            'code' => $this->itemCode,
            'artist' => Slug::generate($this->artist),
            'title' => $this->title,
            'length' => $length,
        ];

        try {
            $id = Api::addSong($data);

        } catch (UnmetDependenciesException $e) {
            $this->publishArtist();

            $id = Api::addSong($data);
        }

        return $id;
    }

    /**
     * @return string
     * @throws ApplicationException
     * @throws ResponseException
     * @throws ParameterMissingException
     * @throws UnmetDependenciesException
     */
    public function publishArtist()
    {
        $this->checkRequirements(['artist']);

        $data = [
            'code' => Slug::generate($this->artist),
            'name' => $this->artist,
            'list' => true,
        ];

        return Api::addArtist($data);
    }

    /**
     * @param array $requirements
     * @throws ParameterMissingException
     */
    protected function checkRequirements(array $requirements)
    {
        foreach ($requirements as $requirement) {
            if (! isset($this->{$requirement})) {
                throw new ParameterMissingException(sprintf('Parameter %s is required.', $requirement));
            }
        }
    }
}
