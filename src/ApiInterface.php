<?php

namespace SDK;

use SDK\Exception\NotFoundException;

interface ApiInterface
{
    /**
     * Add a new artist to the database
     * @link https://api.marcomprocloud.eu/docs/musicdb.html#_musicdb_artist__post
     *
     * @param  array  $data Artist information fields
     * @return string       Id
     * @throws NotFoundException
     */
    public static function addArtist(array $data);

    /**
     * Add a new song to the database
     * @link https://api.marcomprocloud.eu/docs/musicdb.html#_musicdb_song__post
     *
     * @param  array  $data Song information fields
     * @return string       Id
     * @throws NotFoundException
     */
    public static function addSong(array $data);

    /**
     * Add a new station to the database
     * @link https://api.marcomprocloud.eu/docs/musicdb.html#_musicdb_playlist__playlistName__put
     *
     * @param  string $station Reference name
     * @param  array  $data    Station information fields
     * @return string          Id
     * @throws NotFoundException
     */
    public static function addStation($station, array $data);

    /**
     * Add a new song a station is playing
     * @link https://api.marcomprocloud.eu/docs/musicdb.html#_musicdb_playlist__playlistName__song_post
     *
     * @param  string $station   Reference name
     * @param  array  $data      Song information fields
     * @return string            Id on success, false on fail
     * @throws NotFoundException
     */
    public static function addStationSong($station, array $data);

    /**
     * Add the new next song a station will be playing
     * @link https://api.marcomprocloud.eu/docs/musicdb.html#_musicdb_playlist__playlistName__next_song_post
     *
     * @param  string $station   Reference name
     * @param  array  $data      Song information fields
     * @return string            Id on success, false on fail
     * @throws NotFoundException
     */
    public static function setStationNextSong($station, array $data);

    /**
     * Delete the new next song a station will be playing
     * @link https://api.marcomprocloud.eu/docs/musicdb.html#_musicdb_playlist__playlistName__next_song_delete
     *
     * @param  string  $station Station name
     * @return boolean          True
     * @throws NotFoundException
     */
    public static function deleteStationNextSong($station);
}
