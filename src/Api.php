<?php

namespace SDK;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\TooManyRedirectsException;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use SDK\Exception\ApplicationException;
use SDK\Exception\AuthenticationException;
use SDK\Exception\ResponseException;
use SDK\Exception\UnmetDependenciesException;
use SDK\Util\Config;
use SDK\Util\Slug;

class Api implements ApiInterface
{
    /**
     * @var string
     */
    static $authorizationToken;

    /**
     * @var \DateTime
     */
    static $authorizationTokenExpireTime;

    /**
     * @var
     */
    static $client;

    /**
     * @param array $data
     * @return mixed
     * @throws ResponseException
     * @throws UnmetDependenciesException
     */
    public static function addArtist(array $data)
    {
        $slug = Config::getInstance()->get('location')['add_artist_slug'];

        $responseJson = self::doRequest('post', $slug, $data, 201);

        return $responseJson['artistId'];
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ResponseException
     * @throws UnmetDependenciesException
     */
    public static function addSong(array $data)
    {
        $slug = Config::getInstance()->get('location')['add_song_slug'];

        $responseJson = self::doRequest('post', $slug, $data, 201, ['musicdb_invalid_artist_reference']);

        return $responseJson['songId'];
    }

    /**
     * @param string $station
     * @param array $data
     * @return mixed
     * @throws ApplicationException
     * @throws ResponseException
     */
    public static function addStation($station, array $data)
    {
        $slug = self::buildSlug(Config::getInstance()->get('location')['add_station_slug'], ['playlistName' => $station]);

        $responseJson = self::doRequest('put', $slug, $data, [200, 201]);

        return $responseJson['playlistId'];
    }

    /**
     * @param string $station
     * @param array $data
     * @return mixed
     * @throws ApplicationException
     * @throws ResponseException
     * @throws UnmetDependenciesException
     */
    public static function addStationSong($station, array $data)
    {
        $slug = self::buildSlug(Config::getInstance()->get('location')['add_station_song_slug'], ['playlistName' => $station]);

        $responseJson = self::doRequest('post', $slug, $data, 201, ['musicdb_invalid_playlist_reference', 'musicdb_invalid_song_reference']);

        return $responseJson['playlistSongId'];
    }

    /**
     * @param string $station
     * @param array $data
     * @return boolean
     * @throws ApplicationException
     * @throws ResponseException
     * @throws UnmetDependenciesException
     */
    public static function setStationNextSong($station, array $data)
    {
        $slug = self::buildSlug(Config::getInstance()->get('location')['add_station_next_song_slug'], ['playlistName' => $station]);

        self::doRequest('post', $slug, $data, 201, ['musicdb_invalid_playlist_reference', 'musicdb_invalid_song_reference']);

        return true;
    }

    /**
     * @param string $station
     * @return bool
     * @throws ApplicationException
     * @throws ResponseException
     */
    public static function deleteStationNextSong($station)
    {
        $slug = self::buildSlug(Config::getInstance()->get('location')['delete_next_song_slug'], ['playlistName' => $station]);

        self::doRequest('delete', $slug, null, 204);

        return true;
    }

    /**
     * @return string
     */
    public static function getAuthorizationToken()
    {
        if (! self::$authorizationToken || ! self::$authorizationTokenExpireTime || self::$authorizationTokenExpireTime < time()) {
            self::fetchAuthorizationToken();
        }

        return self::$authorizationToken;
    }

    /**
     * @throws AuthenticationException
     * @throws ResponseException
     * @throws UnmetDependenciesException
     */
    protected static function fetchAuthorizationToken()
    {
        $slug = Config::getInstance()->get('location')['authenticate'];
        $authentication = Config::getInstance()->get('authentication');
        $data = [
                'username' => $authentication['username'],
                'password' => $authentication['password'],
                'account'  => $authentication['account'],
        ];

        $response = self::doRequest('post', $slug, $data, 200, [], false);
        self::$authorizationToken = $response['access_token'];
        self::$authorizationTokenExpireTime = $response['expires_at'];
    }

    /* internal methods */

    /**
     * @param string $slug Slug, optionally with {variables}
     * @param array $slugData Slug variable data
     * @return string
     * @throws ApplicationException
     */
    protected static function buildSlug($slug, array $slugData = array())
    {
        if (preg_match_all('/\{\s*([a-z0-9_\.-]+)\s*\}/i', $slug, $matches)) {
            foreach ($matches[0] as $key => $match) {
                if (! isset($slugData[$matches[1][$key]])) {
                    throw new ApplicationException('Slug parameter data not set: ' . $matches[1][$key]);
                }

                $slug = str_replace($match, Slug::generate($slugData[$matches[1][$key]]), $slug);
            }
        }

        return $slug;
    }

    /**
     * Get a client instance
     *
     * @param bool $authorize
     * @return Client
     */
    protected static function getClient($authorize = true)
    {
        $config = [
            'base_uri' => Config::getInstance()->get('location')['base_uri'],
            'headers' => [
                    'User-Agent'    => '100pnl-musicdb-sdk/0.1',
                    'Accept'        => 'application/json',
            ],
            'verify' => (bool) Config::getInstance()->get('other')['verify_ssl_certificate'],
        ];

        if ($authorize) {
            $config['headers']['Authorization'] = 'Token ' . self::getAuthorizationToken();
        }

        self::insertTestingHeaders($config['headers']);

        $client = new Client($config);

        return $client;
    }

    /**
     * @param string $method
     * @param string $slug
     * @param array $data
     * @param int|array $acceptResponseCodes
     * @param array $unmetDependencyCodes
     * @param int $retryCounter
     * @return mixed
     * @throws AuthenticationException
     * @throws ResponseException
     * @throws UnmetDependenciesException
     */
    protected static function doRequest($method, $slug, array $data = null, $acceptResponseCodes, array $unmetDependencyCodes = array(), $authorize = true, $retryCounter = 0)
    {
        try {
            $client = self::getClient($authorize);

//            printf("- Requesting %s %s with body %s and headers %s\n\n", strtoupper($method), $slug, json_encode($data), json_encode($client->getConfig('headers')));

            if (null === $data) {
                /** @var Response $response */
                $response = $client->$method($slug);
            } else {
                /** @var Response $response */
                $response = $client->$method($slug, ['json' => $data]);
            }

        } catch (TransferException $e) {
            if ($e instanceof ClientException) {
                // 400 level error, do not retry
                $responseCode = $e->getResponse()->getStatusCode();
                $responseJson = $e->getResponse()->getBody()->getContents();
                $responseArr = self::getResponseJson($responseJson);

                if (in_array($responseCode, [401, 403, 429])) {
                    throw new AuthenticationException(sprintf('(slug: %s, code: %d) %s', $slug, $responseCode, $responseArr['msg']), null, $e);
                }

                if (isset($responseArr['code']) && in_array($responseArr['code'], $unmetDependencyCodes)) {
                    throw new UnmetDependenciesException($responseArr['code']/*, null, $e*/);
                }

                throw new ResponseException(sprintf('Unexpected response: (slug: %s, code: %s) %s', $slug, $responseCode, $responseJson), null, $e);

            } elseif ($e instanceof ServerException ) {
                // 500 level error, do not retry

            } elseif ($e instanceof TooManyRedirectsException) {
                // do not retry

            } elseif ($e instanceof RequestException) {
                // all other networking exceptions, retry
                if (2 > $retryCounter) {
                    sleep(($retryCounter+1)^2); // wait 1 sec before first retry, 4 sec before second

                    return self::doRequest($method, $slug, $data, $acceptResponseCodes, $unmetDependencyCodes, $authorize, $retryCounter + 1);
                } else {
                    throw new ResponseException('Could not get a response'/*, null, $e*/);
                }
            }

            throw $e;
        }

        $responseCode = $response->getStatusCode();
        if (! in_array($responseCode, (array) $acceptResponseCodes)) {
            throw new ResponseException(
                    sprintf(
                            "Unexpected response. Expected one of the following codes: %s. Got %d (%s), with response body: %s.",
                            implode(',', (array) $acceptResponseCodes),
                            $responseCode,
                            $response->getReasonPhrase(),
                            $response->getBody()->getContents()
                    )
            );
        }

        if (204 !== $response) {
            return self::getResponseJson($response);
        } else {
            return null;
        }
    }

    /**
     * @param ResponseInterface|string $response
     * @return mixed
     * @throws ResponseException
     */
    protected static function getResponseJson($response)
    {
        if ($response instanceof ResponseInterface) {
            $response = $response->getBody()->getContents();
        }

        $responseArr = json_decode($response, true);

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new ResponseException('Invalid json response: '.$response);
        }

        return $responseArr;
    }

    /**
     * Inject test headers when testing
     * @param array $headers
     */
    private static function insertTestingHeaders(array &$headers)
    {
        $authentication = Config::getInstance()->get('authentication');

        if (isset($authentication['unitTestKey'])) {
            $key  = $authentication['unitTestKey'];
            $time = time();
            $unitTestToken = base64_encode($time . '|' . hash_hmac('sha256', $time, $key));
            $headers['x-mcp-unit-testing'] = $unitTestToken;
        }
    }
}
