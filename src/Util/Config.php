<?php

namespace SDK\Util;

class Config
{
    /**
     * @var Config
     */
    protected static $instance;

    /**
     * @var array
     */
    protected $config = [];

    /**
     * @return Config
     */
    public static function getInstance()
    {
        if (! (self::$instance instanceof self))
        {
            new static;
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param string $file
     */
    public function loadConfigFile($file = 'production')
    {
        $this->config = parse_ini_file(realpath(__DIR__ . '/../Config/' . $file . '.ini'), true);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        if (! $this->config) {
            $this->loadConfigFile();
        }

        if (! array_key_exists($key, $this->config)) {
            return null;
        }

        return $this->config[$key];
    }

    /* make sure it's a singleton */
    private function __construct() {}
    private function __clone() {}
    private function __wakeup() {}
}
