<?php

namespace SDK\Util;

use SDK\Exception\ApplicationException;

/**
 * @author Samuel Carlier
 */
class Slug
{
    /**
     * Creates a slug to be used for pretty URLs.
     *
     * @link http://cubiq.org/the-perfect-php-clean-url-generator
     *
     * @param  string $string
     * @param  string $delimiter (optional, '-')
     * @return string
     * @throws ApplicationException
     */
    public static function generate($string, $delimiter = '-')
    {
        if (! extension_loaded('iconv')) {
            throw new ApplicationException('iconv module not loaded');
        }

        // Save the old locale and set the new locale to UTF-8
        $oldLocale = setlocale(LC_ALL, '0');
        setlocale(LC_ALL, 'en_US.UTF-8');

        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
        $clean = preg_replace("#[^a-zA-Z0-9/_|+ -]+#", '', $clean);
        $clean = strtolower($clean);

        $ambiguous = ['-', '_'];
        $replace = '';
        if (! in_array($delimiter, $ambiguous)) {
            $replace = preg_quote(implode('', $ambiguous), '#');
        }
        $clean = preg_replace("#[/|+ $replace]+#", $delimiter, $clean);
        $clean = trim($clean, $delimiter);

        // Revert back to the old locale
        setlocale(LC_ALL, $oldLocale);

        return $clean;
    }
}
