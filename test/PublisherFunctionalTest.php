<?php

namespace SDK\test;

use SDK\Exception\UnmetDependenciesException;
use SDK\Publisher;

class PublisherFunctionalTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @throws UnmetDependenciesException
     * @throws \Exception
     */
    public function testPublishStationSong()
    {
        $publisher = (new Publisher())
            ->setStation('spring2015')
            ->setStartTime(new \DateTime('2015-06-03 16:50'))
            ->setItemCode('testsong')
            ->setArtist('testartist')
            ->setTitle('testtitle')
            ->setLength(new \DateInterval('PT3M12S'))
            ->setJockTitle('SomeDJ')
        ;

        $id = $publisher->publishStationSong();
        $this->assertEquals(24, strlen($id), 'Invalid response id: '.$id);
    }

    /**
     *
     */
    public function testPublishStationNextSong()
    {
        $publisher = (new Publisher())
                ->setStation('spring2015')
                ->setStartTime(new \DateTime('2015-06-03 16:53'))
                ->setItemCode('testsong')
                ->setArtist('testartist')
                ->setTitle('testtitle')
                ->setLength(new \DateInterval('PT3M12S'))
                ->setJockTitle('SomeDJ')
        ;

        $successState = $publisher->publishStationNextSong();
        $this->assertEquals(true, $successState, 'Invalid response: ' . var_export($successState, true));
    }
}
