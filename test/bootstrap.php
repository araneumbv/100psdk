<?php

$base = realpath(__DIR__ . '/..');

require $base . '/vendor/autoload.php';

\SDK\Util\Config::getInstance()->loadConfigFile('unittest');
