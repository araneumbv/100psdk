100pSDK
=======
___
Adding a song to a station's playlist
------------------------------------
Publish a song that is playing at station "MyStationName":

```php
// First, create a Publisher object and set your data
$publisher = (new \SDK\Publisher())
    // Name of the station
    ->setStation('MyStationName')
    // The time the song started playing
    ->setStartTime(new \DateTime('2015-06-03 16:50'))
    // Unique code that identifies the song
    ->setItemCode('testsong')
    // The name of the artist
    ->setArtist('testartist')
    // The song's title
    ->setTitle('testtitle')
    // The song's duration
    ->setLength(new \DateInterval('PT3M12S'))
    // Your jock title
    ->setJockTitle('SomeDJ')
;

// Then, publish that the song has been played
// The method returns the id of the inserted song play instance
$id = $publisher->publishStationSong();
```

The song and artist on which the playlist song depends are published automatically if they do not exist.

Adding a next song to a station's playlist
-----------------------------------------
Publishing a the song that will play next at station "MyStationName" is very similar to the previous example:

```php
// First, create a Publisher object and set your data
$publisher = (new \SDK\Publisher())
    // Name of the station
    ->setStation('MyStationName')
    // The time the song started playing
    ->setStartTime(new \DateTime('2015-06-03 16:50'))
    // Unique code that identifies the song
    ->setItemCode('testsong')
    // The name of the artist
    ->setArtist('testartist')
    // The song's title
    ->setTitle('testtitle')
    // The song's duration
    ->setLength(new \DateInterval('PT3M12S'))
    // Your jock title
    ->setJockTitle('SomeDJ')
;

// Then, publish that the song will be played next
// The method returns boolean true
$publisher->publishStationNextSong();                   
```

The song and artist on which the playlist song depends are published automatically if they do not exist.